# Reference link: https://iridakos.com/programming/2019/04/07/dockerizing-a-rails-application
FROM ruby:2.1

# Copy application code
COPY . /application

# Change to the application's directory
WORKDIR /application

# Set Rails environment to production
ENV RAILS_ENV production

# Install dependencies.
RUN apt-get update -qy \
    && curl -sL https://deb.nodesource.com/setup_10.x | bash - \
    && apt install -y nodejs \
    && wget -O /usr/local/bin/dumb-init https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 \
    && chmod +x /usr/local/bin/dumb-init

# Setup the app
RUN bundle install --without development test \
    && bundle exec rake assets:precompile \
    && bundle exec rake db:create db:migrate \
    && chmod +x entrypoint.sh

EXPOSE 5000

#Dumb-init -> https://github.com/Yelp/dumb-init
ENTRYPOINT ["/usr/local/bin/dumb-init", "--"]

# Start the server
CMD ["bash", "entrypoint.sh"]