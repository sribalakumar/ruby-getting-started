# Remove the ENV passed from the k8s deploy - https://gitlab.com/sribalakumar/kubernetes-deploy/-/blob/master/deploy#L170
unset DATABASE_URL

# Start the server
bundle exec rails server -b 0.0.0.0 -p 5000